#include "i2c_slave_functions.h"
/**
 * Use memory_index and read the data to *memory pointer
 * return true if everything is well
 */
bool i2c_slave_callback__read_memory(uint8_t memory_index, uint8_t *memory) {
  bool status = false;
  if (memory_index > 256) {
    status = false;
  } else {
    *memory = slave_memory[memory_index];
    printf("Inside Read Function at index %d : %d\n", memory_index);
    status = true;
  }
  return status;
}

/**
 * Use memory_index to write memory_value
 * return true if this write operation was valid
 */
bool i2c_slave_callback__write_memory(uint8_t memory_index, uint8_t memory_value) {
  bool status = false;
  if (memory_index > 256) {
    status = false;
  } else {
    printf("Inside write function\n");
    slave_memory[memory_index] = memory_value;
    status = true;
  }
  return status;
}