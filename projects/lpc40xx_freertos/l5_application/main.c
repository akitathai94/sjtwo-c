#include "DEV_Config.h"
#include "EPD_2in9.h"
#include "FreeRTOS.h"
#include "GUI_Paint.h"
#include "app_cli.h"
#include "ff.h"
#include "fonts.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include "mp3_module.h"
#include "queue.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "song_list.h"
#include "ssp0.h"
#include "task.h"
#include <stdio.h>
#include <string.h>
// Struct Definition for Input Pin Button
typedef struct {
  uint8_t port;
  uint8_t pin;
} port_pin_s;
typedef char songname_t[128];
// Create a new image cache
UBYTE *BlackImage;
// vector<songname_t> songList;
typedef uint8_t mp3_data_blocks_t[512];
QueueHandle_t Q_songname;
QueueHandle_t Q_songdata;

// Semaphore Definition
SemaphoreHandle_t play_pause_Semaphore;
SemaphoreHandle_t play_next_Semaphore;
SemaphoreHandle_t play_prev_Semaphore;
SemaphoreHandle_t mode_Semaphore;
SemaphoreHandle_t volume_up_Semaphore;
SemaphoreHandle_t volume_down_Semaphore;
SemaphoreHandle_t select_Semaphore;
SemaphoreHandle_t isDone_Semaphore;
SemaphoreHandle_t is_counting;
// Task handle Definition
TaskHandle_t mp3Player_Handle = NULL;
TaskHandle_t time_counting_Handle = NULL;
//  MP3 Decoder
void mp3_player_task();
void mp3_reader_task();

// LCD display
void LCD_display_task();

// Button Task
void button_task();
void button_action_handle();

// Time counting for song
void time_counting();
int main(void) {
  uint16_t sci_mode;
  mp3_init();
  sci_mode = read_register(SCI_VOL);
  printf("VOL: %d\n", sci_mode);
  sj2_cli__init();
  // Testing song_list
  song_list__populate();
  // Testing LCD
  printf("EPD_2IN9_test Demo\r\n");
  if (DEV_Module_Init() != 0) {
    return -1;
  }
  printf("e-Paper Init and Clear...\r\n");
  EPD_2IN9_Init(EPD_2IN9_FULL);
  EPD_2IN9_Clear();
  EPD_2IN9_Init(EPD_2IN9_PART);
  DEV_Delay_ms(500);

  /* you have to edit the startup_stm32fxxx.s file and set a big enough heap size */
  UWORD Imagesize = ((EPD_2IN9_WIDTH % 8 == 0) ? (EPD_2IN9_WIDTH / 8) : (EPD_2IN9_WIDTH / 8 + 1)) * EPD_2IN9_HEIGHT;
  if ((BlackImage = (UBYTE *)malloc(Imagesize)) == NULL) {
    printf("Failed to apply for black memory...\r\n");
    return -1;
  }
  printf("Paint_NewImage\r\n");
  Paint_NewImage(BlackImage, EPD_2IN9_WIDTH, EPD_2IN9_HEIGHT, 270, WHITE);
  printf("Drawing\r\n");
  // 1.Select Image
  Paint_SelectImage(BlackImage);
  Paint_Clear(WHITE);
  Paint_DrawString_EN(10, 50, "VOLUME", &Font16, WHITE, BLACK);
  Paint_DrawNum(90, 50, getCurrentVolume(), &Font16, WHITE, BLACK);
  // 2.Drawing on the image
  printf("Drawing:BlackImage\r\n");
  // Paint_DrawPoint(10, 80, BLACK, DOT_PIXEL_1X1, DOT_STYLE_DFT);
  // Paint_DrawPoint(10, 90, BLACK, DOT_PIXEL_2X2, DOT_STYLE_DFT);
  // Paint_DrawPoint(10, 100, BLACK, DOT_PIXEL_3X3, DOT_STYLE_DFT);

  // Paint_DrawLine(20, 70, 70, 120, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);
  // Paint_DrawLine(70, 70, 20, 120, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);

  // Paint_DrawRectangle(20, 70, 70, 120, BLACK, DOT_PIXEL_1X1, DRAW_FILL_EMPTY);
  // Paint_DrawRectangle(80, 70, 130, 120, BLACK, DOT_PIXEL_1X1, DRAW_FILL_FULL);

  // Paint_DrawCircle(45, 95, 20, BLACK, DOT_PIXEL_1X1, DRAW_FILL_EMPTY);
  // Paint_DrawCircle(105, 95, 20, WHITE, DOT_PIXEL_1X1, DRAW_FILL_FULL);

  // Paint_DrawLine(85, 95, 125, 95, BLACK, DOT_PIXEL_1X1, LINE_STYLE_DOTTED);
  // Paint_DrawLine(105, 75, 105, 115, BLACK, DOT_PIXEL_1X1, LINE_STYLE_DOTTED);
  // Paint_DrawString_EN(10, 0, "waveshare", &Font16, BLACK, WHITE);

  // Paint_DrawNum(10, 33, 123456789, &Font12, BLACK, WHITE);
  // Paint_DrawNum(10, 50, 987654321, &Font16, WHITE, BLACK);
  DEV_Delay_ms(500);
  // Initialize Semaphore for Buttons
  volume_up_Semaphore = xSemaphoreCreateBinary();
  volume_down_Semaphore = xSemaphoreCreateBinary();
  play_next_Semaphore = xSemaphoreCreateBinary();
  play_prev_Semaphore = xSemaphoreCreateBinary();
  play_pause_Semaphore = xSemaphoreCreateBinary();
  is_counting = xSemaphoreCreateBinary();
  // Testing for display
  for (size_t song_number = 0; song_number < song_list__get_item_count(); song_number++) {
    printf("Song %2d: %s\n", (1 + song_number), song_list__get_name_for_item(song_number));
  }
  Q_songname = xQueueCreate(1, sizeof(songname_t));
  Q_songdata = xQueueCreate(3, sizeof(mp3_data_blocks_t));
  xTaskCreate(mp3_reader_task, "mp3reader", 4000 / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(mp3_player_task, "mp3player", 4000 / sizeof(void *), NULL, PRIORITY_MEDIUM, &mp3Player_Handle);
  xTaskCreate(button_task, "button_task", 1000 / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  xTaskCreate(button_action_handle, "button_action_handle", 1000 / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  xTaskCreate(LCD_display_task, "lcd_task", 1000 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(time_counting, "time_counting", 1000 / sizeof(void *), NULL, PRIORITY_LOW, &time_counting_Handle);

  vTaskStartScheduler();
  return 0;
}
app_cli_status_e cli__mp3_play(app_cli__argument_t argument, sl_string_t user_input_minus_command_name,
                               app_cli__print_string_function cli_output) {
  // user_input_minus_command_name is actually a 'char *' pointer type
  // We tell the Queue to copy 32 bytes of songname from this location
  songname_t songname = {0};
  memset(songname, 0, sizeof(songname));
  if (sl_string__get_length(user_input_minus_command_name) > sizeof(songname)) {
    printf("Unable to play the song because songname is too long \n");
  } else {
    strncpy(songname, user_input_minus_command_name, sizeof(songname) - 1);
    xQueueSend(Q_songname, songname, portMAX_DELAY);
    printf("Sent %s over to the Q_songname\n", user_input_minus_command_name);
  }
  return APP_CLI_STATUS__SUCCESS;
}
// Reader tasks receives song-name over Q_songname to start reading it
void mp3_reader_task(void *p) {
  songname_t name;
  mp3_data_blocks_t mp3_data_block;
  while (1) {
    if (xQueueReceive(Q_songname, name, portMAX_DELAY)) {
      // open_file();
      FIL file;
      FRESULT fr;
      UINT br;
      const char *filename = name;
      printf("Received song to play: %s\n", name);
      fr = f_open(&file, filename, FA_READ);
      if (fr) {
        printf("Cannot open a song %s\n", name);
        printf("Error value %d\n", (int)fr);
      } else {
        // read file();
        while (!f_eof(&file)) {
          f_read(&file, mp3_data_block, sizeof(mp3_data_block), &br);
          if (br == 0)
            break;
          xQueueSend(Q_songdata, mp3_data_block, portMAX_DELAY);
          f_read(&file, mp3_data_block, sizeof(mp3_data_block), &br);
          if (br == 0)
            break;
          xQueueSend(Q_songdata, mp3_data_block, portMAX_DELAY);
          // printf("Sent %dth byte(s) to play:\n", br);
          if (br == 0 || uxQueueMessagesWaiting(Q_songname)) {
            break;
            mp3_config.isDone = false;
          }
        }
        // finish playing the file
        if (f_eof(&file)) {
          mp3_config.isDone = true;
        }
        if (mp3_config.isDone) {
          mp3_config.isDone = false;
          mp3_config.isPlaying = false;
          xSemaphoreGive(play_next_Semaphore);
        }
      }
    }
  }
}
// Player task receives song data over Q_songdata to send it to the MP3 decoder
void mp3_player_task(void *p) {
  mp3_data_blocks_t mp3_data_block;
  while (1) {
    if (xQueueReceive(Q_songdata, mp3_data_block, portMAX_DELAY)) {
      // puts("Inside Queue\n");
      size_t byte_count = 0;
      while (byte_count < sizeof(mp3_data_block)) {
        data_line_request();
        for (size_t byte = byte_count; byte < (byte_count + 32); byte++) {
          // printf("%x", mp3_data_block[byte]);
          spi_send_to_mp3_decoder(mp3_data_block[byte]);
        }
        // check dreq after 32 bytes
        data_line_request();
        byte_count += 32;
        vTaskDelay(2);
      }
    }
    // puts("Done reading 512 bytes\n");
  }
}

void button_task() {
  // Instantiate Port and Pin for buttons wit Struct
  port_pin_s volumeUp = {2, 1};
  port_pin_s volumeDown = {2, 4};
  port_pin_s play_pause_Button = {2, 6};
  port_pin_s play_next_Button = {2, 8};
  port_pin_s play_prev_Button = {1, 29};

  // Set up direction for buttons
  gpio0__set_as_input(volumeUp.port, volumeUp.pin);
  gpio0__set_as_input(volumeDown.port, volumeDown.pin);
  gpio0__set_as_input(play_pause_Button.port, play_pause_Button.pin);
  gpio0__set_as_input(play_next_Button.port, play_next_Button.pin);
  gpio0__set_as_input(play_prev_Button.port, play_pause_Button.pin);
  // Activate pull down for sw0 and sw1
  LPC_IOCON->P1_15 &= ~(0b111 << 3);
  LPC_IOCON->P1_19 &= ~(0b111 << 3);
  LPC_IOCON->P1_15 |= (0b010 << 3);
  LPC_IOCON->P1_19 |= (0b010 << 3);

  // Button Flags
  bool volumeUp_pressed = false;
  bool volumeDown_pressed = false;
  bool play_next_pressed = false;
  bool play_pause_pressed = false;
  bool play_prev_pressed = false;
  while (1) {
    // Volume Up check
    if (gpio0__get_level(volumeUp.port, volumeUp.pin)) {
      volumeUp_pressed = true;
      puts("Up PRESSED\n");
    }
    if (volumeUp_pressed) {
      if (!gpio0__get_level(volumeUp.port, volumeUp.pin)) {
        volumeUp_pressed = false;
        xSemaphoreGive(volume_up_Semaphore);
      }
    }
    // Volume Down Check
    if (gpio0__get_level(volumeDown.port, volumeDown.pin)) {
      volumeDown_pressed = true;
      puts("Down PRESSED\n");
    }
    if (volumeDown_pressed) {
      if (!gpio0__get_level(volumeDown.port, volumeDown.pin)) {
        volumeDown_pressed = false;
        xSemaphoreGive(volume_down_Semaphore);
      }
    }
    // Play Next Check
    if (gpio0__get_level(play_next_Button.port, play_next_Button.pin)) {
      play_next_pressed = true;
      puts("Next PRESSED\n");
    }
    if (play_next_pressed) {
      if (!gpio0__get_level(play_next_Button.port, play_next_Button.pin)) {
        play_next_pressed = false;
        xSemaphoreGive(play_next_Semaphore);
      }
    }
    // Play Prev Check
    if (gpio0__get_level(play_prev_Button.port, play_prev_Button.pin)) {
      play_prev_pressed = true;
      puts("Prev PRESSED\n");
    }
    if (play_prev_pressed) {
      if (!gpio0__get_level(play_prev_Button.port, play_prev_Button.pin)) {
        play_prev_pressed = false;
        xSemaphoreGive(play_prev_Semaphore);
      }
    }
    // Play Pause Check (if Play is true it should play continously through the list)
    if (gpio0__get_level(play_pause_Button.port, play_pause_Button.pin)) {
      play_pause_pressed = true;
      puts("Play Pause PRESSED\n");
    }
    if (play_pause_pressed) {
      if (!gpio0__get_level(play_pause_Button.port, play_pause_Button.pin)) {
        play_pause_pressed = false;
        xSemaphoreGive(play_pause_Semaphore);
      }
    }
    vTaskDelay(100);
  }
}

void button_action_handle() {
  songname_t name;
  while (1) {
    if (xSemaphoreTake(volume_up_Semaphore, 1)) {
      volumeUp();
      puts("Volume up\n");
      Paint_DrawNum(90, 50, mp3_config.volumeCurrent, &Font16, WHITE, BLACK);
    }
    if (xSemaphoreTake(volume_down_Semaphore, 1)) {
      Paint_DrawNum(90, 50, mp3_config.volumeCurrent, &Font16, WHITE, BLACK);
      volumeDown();
      puts("Volume down\n");
    }
    // Handle play next button
    if (xSemaphoreTake(play_next_Semaphore, 1)) {
      // Activate isPlaying case
      mp3_config.isPlaying = true;
      mp3_config.isPause = false;
      // If reach the end of list song reset to 0
      if ((mp3_config.currentSong + 1) > (song_list__get_item_count() - 1)) {
        mp3_config.currentSong = 0;
      } else {
        mp3_config.currentSong++;
      }
      const char *filename = song_list__get_name_for_item(mp3_config.currentSong);
      Paint_ClearWindows(10, 20, 200 + Font16.Width * 7, 20 + Font16.Height, BLACK);
      Paint_DrawString_EN(10, 20, filename, &Font16, BLACK, WHITE);
      strncpy(name, filename, sizeof(name) - 1);
      xQueueSend(Q_songname, name, 1);
      xSemaphoreGive(is_counting);
    }
    // Handle play prev button
    if (xSemaphoreTake(play_prev_Semaphore, 1)) {
      // Activate isPlaying case
      mp3_config.isPlaying = true;
      mp3_config.isPause = false;
      // If reach the first song then go to last song
      if ((mp3_config.currentSong) <= 0) {
        mp3_config.currentSong = song_list__get_item_count() - 1;
      } else {
        mp3_config.currentSong--;
      }
      const char *filename = song_list__get_name_for_item(mp3_config.currentSong);
      Paint_ClearWindows(10, 20, 200 + Font16.Width * 7, 20 + Font16.Height, BLACK);
      Paint_DrawString_EN(10, 20, filename, &Font16, BLACK, WHITE);
      strncpy(name, filename, sizeof(name) - 1);
      xQueueSend(Q_songname, name, 1);
      xSemaphoreGive(is_counting);
    }
    // Handle play pause button
    if (xSemaphoreTake(play_pause_Semaphore, 1)) {
      if (mp3_config.isPlaying) {
        mp3_config.isPlaying = false;
        mp3_config.isPause = true;
        // Pause playing
        puts("Pause\n");
        vTaskSuspend(time_counting_Handle);
        vTaskSuspend(mp3Player_Handle);
        Paint_ClearWindows(10, 0, Font16.Width * 12, Font16.Height, WHITE);
        Paint_DrawString_EN(10, 0, "PAUSE", &Font16, WHITE, BLACK);
      } else if (mp3_config.isPause) {
        mp3_config.isPlaying = true;
        mp3_config.isPause = false;
        puts("Resume\n");
        Paint_ClearWindows(10, 0, Font16.Width * 12, Font16.Height, WHITE);
        Paint_DrawString_EN(10, 0, "NOW PLAYING", &Font16, WHITE, BLACK);
        vTaskResume(mp3Player_Handle);
        vTaskResume(time_counting_Handle);
      } else if ((!mp3_config.isPlaying) && (!mp3_config.isPause)) {
        // Is not pause play a current song
        // Play action send Semaphore to play the current song
        puts("Play song\n");
        mp3_config.isPlaying = true;
        const char *filename = song_list__get_name_for_item(mp3_config.currentSong);
        Paint_ClearWindows(10, 0, Font16.Width * 12, Font16.Height, WHITE);
        Paint_DrawString_EN(10, 0, "NOW PLAYING", &Font16, WHITE, BLACK);
        Paint_ClearWindows(10, 20, 200 + Font16.Width * 7, 20 + Font16.Height, BLACK);
        Paint_DrawString_EN(10, 20, filename, &Font16, BLACK, WHITE);
        strncpy(name, filename, sizeof(name) - 1);
        xQueueSend(Q_songname, name, 1);
        xSemaphoreGive(is_counting);
      }
    }
    vTaskDelay(100);
  }
}

// LCD display Task
void LCD_display_task() {
  while (1) {
    EPD_2IN9_Display(BlackImage);
    vTaskDelay(1000);
  }
}

// Time counting for song
void time_counting() {
  while (1) {
    PAINT_TIME sPaint_time;
    sPaint_time.Hour = 0;
    sPaint_time.Min = 0;
    sPaint_time.Sec = 0;
    for (;;) {
      sPaint_time.Sec = sPaint_time.Sec + 1;
      if (sPaint_time.Sec == 60) {
        sPaint_time.Min = sPaint_time.Min + 1;
        sPaint_time.Sec = 0;
        if (sPaint_time.Min == 60) {
          sPaint_time.Hour = sPaint_time.Hour + 1;
          sPaint_time.Min = 0;
          if (sPaint_time.Hour == 24) {
            sPaint_time.Hour = 0;
            sPaint_time.Min = 0;
            sPaint_time.Sec = 0;
          }
        }
        if (xSemaphoreTake(is_counting, 1)) {
          break;
        }
        Paint_ClearWindows(150, 80, 150 + Font20.Width * 7, 80 + Font20.Height, WHITE);
        Paint_DrawTime(150, 80, &sPaint_time, &Font20, WHITE, BLACK);
        vTaskDelay(2000); // Analog clock 1s
      }
    }
  }
}
