#pragma once

#include "FreeRTOS.h"
#include "delay.h"
#include "gpio.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include "ssp0.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "task.h"

#define SCI_MODE 0x00
#define SCI_STATUS 0x01
#define SCI_BASS 0x02
#define SCI_CLOCKF 0x03
#define SCI_DECODE_TIME 0x04
#define SCI_AUDATA 0x05
#define SCI_WRAM 0x06
#define SCI_WRAMADDR 0x07
#define SCI_HDAT0 0x08
#define SCI_HDAT1 0x09
#define SCI_AIADDR 0x0A
#define SCI_VOL 0x0B
#define SCI_AICTRL0 0x0C
#define SCI_AICTRL1 0x0D
#define SCI_AICTRL2 0x0E
#define SCI_AICTRL3 0x0F

// SCI_BASS configure
#define BASS_EFFECT 0x00FF  // enhanced BASS configure for SCI_BASS
#define BASS_DEFAULT 0x7A00 // default BASS configure for SCI_BASS

// SCI_MODE configure
#define SCI_DEFAULT 0x4800 // no effect
#define SCI_EFFECT 0x4890  // LO HIGH earspeaker (heavy effect)

// SCI_VOL configure
#define VOL_DEFAULT 0x5050

// SCI_CLOCKF configure
#define CLOCKF_DEFAULT 0xA000 // Multiply the frequency by 4 (12 x 4 ==  48)

// READ and WRITE command
#define READ 0x03
#define WRITE 0x02
// Signals
#define HIGH 1
#define LOW 0

// MP3 pins with PORT = 2
// #define MP3_PORT 2
// #define MP3_DREQ 2 // Data Request  (P2.2)
// #define MP3_CS 0   // Command CS    (P2.0) Active Low
// #define MP3_DCS 5  // Data CS       (P2.5) Active Low
// #define MP3_RST 23 // Reset         (P1.23) Active Low

typedef struct {
  uint8_t port;
  uint8_t cs_pin;
  uint8_t dreq_pin;
  uint8_t dcs_pin;
} mp3_pin_setup;
typedef struct {
  bool mute;
  uint16_t volume;
  uint32_t volumeCurrent;
  bool isPlaying;
  size_t currentSong;
  bool isRepeat;
  bool isPause;
  bool isDone;
} mp3_config_t;
mp3_config_t mp3_config;
const mp3_pin_setup mp3_pin_init;
// Set chip select
void set_CS();
// Reset chip select
void reset_CS();
// Set Data Chip select
void set_xDCS();
// Reset Data chip select
void reset_xDCS();
void mp3_init();
// Write SCI registers
void write_register(uint8_t addr, uint16_t data);
// Read SCI registers
uint16_t read_register(uint8_t addr);
//  If decoder need more data
bool mp3_decoder_needs_data();
// wait for data line available
void data_line_request();
// Send chuck data to decoder
void spi_send_to_mp3_decoder(uint8_t data);

void printDetails();
// Set Bass and Treble
void setTone(int bass_level, int treble_level);
// Volume Up and Down
void volumeUp();
void volumeDown();
// Set Volume for mp3
void setVolume(uint8_t level);
// get current volume from MP3
uint16_t getCurrentVolume();