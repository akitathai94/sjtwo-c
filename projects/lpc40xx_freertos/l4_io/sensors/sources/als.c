#include "als.h"

#include "i2c.h"

static const i2c_e als__sensor_bus = I2C__2;
static const uint8_t als__address = 0x72;
typedef enum {
  als__enable_reg = 0x80,
  als__adc_time = 0x81,
  als__id_reg = 0x92,
  als__status_reg = 0x93,
  als__data_addr_base = 0x94,
  als__cleardata_L = 0x94,
  als__cleardata_H = 0x95
} als__memory_addr;

bool als__init(void) {
  const uint8_t power_up_als = 0x03; // Set bit 0 and bit 1 for power on and enable ALS
  i2c__write_single(als__sensor_bus, als__address, als__enable_reg, power_up_als);

  const uint8_t who_am_i_expected_value = 0xAB;
  const uint8_t who_am_i_actual_value = i2c__read_single(als__sensor_bus, als__address, als__id_reg);
  return (who_am_i_actual_value == who_am_i_expected_value);
}

uint16_t als__get_data(void) {
  uint8_t byte_data[2];
  uint16_t data;
  i2c__read_slave_data(als__sensor_bus, als__address, als__cleardata_L, byte_data, sizeof(byte_data));
  data = ((uint16_t)byte_data[1] << 8) | byte_data[0];
  return data;
}
