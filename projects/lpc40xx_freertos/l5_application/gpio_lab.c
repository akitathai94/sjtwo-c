#include "gpio_lab.h"
#include "lpc40xx.h"

void gpio0__set_as_input(uint8_t port, uint8_t pin_num) {
  switch (port) {
  case 0:
    LPC_GPIO0->DIR &= ~(1 << pin_num);
    break;
  case 1:
    LPC_GPIO1->DIR &= ~(1 << pin_num);
    break;
  case 2:
    LPC_GPIO2->DIR &= ~(1 << pin_num);
    break;
  }
}

void gpio0__set_as_output(uint8_t port, uint8_t pin_num) {
  switch (port) {
  case 0:
    LPC_GPIO0->DIR |= (1 << pin_num);
    break;
  case 1:
    LPC_GPIO1->DIR |= (1 << pin_num);
    break;
  case 2:
    LPC_GPIO2->DIR |= (1 << pin_num);
    break;
  }
}

void gpio0__set_high(uint8_t port, uint8_t pin_num) {
  switch (port) {
  case 0:
    LPC_GPIO0->SET |= (1 << pin_num);
    break;
  case 1:
    LPC_GPIO1->SET |= (1 << pin_num);
    break;
  case 2:
    LPC_GPIO2->SET |= (1 << pin_num);
    break;
  }
}

void gpio0__set_low(uint8_t port, uint8_t pin_num) {
  switch (port) {
  case 0:
    LPC_GPIO0->CLR |= (1 << pin_num);
    break;
  case 1:
    LPC_GPIO1->CLR |= (1 << pin_num);
    break;
  case 2:
    LPC_GPIO2->CLR |= (1 << pin_num);
    break;
  }
}

/**
 * Should alter the hardware registers to set the pin as low
 *
 * @param {bool} high - true => set pin high, false => set pin low
 */
void gpio0__set(uint8_t port, uint8_t pin_num, bool high) {
  if (high) {
    gpio0__set_high(port, pin_num);
  } else {
    gpio0__set_low(port, pin_num);
  }
}
bool gpio0__get_level(uint8_t port, uint8_t pin_num) {
  switch (port) {
  case 0:
    if (LPC_GPIO0->PIN & (1 << pin_num)) {
      return true;
    } else
      return false;
    break;
  case 1:
    if (LPC_GPIO1->PIN & (1 << pin_num)) {
      return true;
    } else
      return false;
    break;
  case 2:
    if (LPC_GPIO2->PIN & (1 << pin_num)) {
      return true;
    } else
      return false;
    break;
  }
}