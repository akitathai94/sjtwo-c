/*****************************************************************************
* | File      	:   DEV_Config.c
* | Author      :   Waveshare team
* | Function    :   Hardware underlying interface
* | Info        :
*----------------
* |	This version:   V3.0
* | Date        :   2019-07-31
* | Info        :
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documnetation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of theex Software, and to permit persons to  whom the Software is
# furished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
******************************************************************************/
#include "DEV_Config.h"
#include <fcntl.h>

#include "delay.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include "ssp1.h"

// Instantiate for LCD  PIN
const e_paper_pin_setup lcd_pin_init = {31, 30, 14, 20};

/**
 * GPIO read and write
 **/
void DEV_Digital_Write(UWORD Pin, UBYTE Value) {
  if (Value == 1) {
    gpio0__set_high(1, Pin);
  } else {
    gpio0__set_low(1, Pin);
  }
}

UBYTE DEV_Digital_Read(UWORD Pin) {
  if (LPC_GPIO1->PIN & (1 << Pin)) {
    return 1;
  } else {
    return 0;
  }
}

/**
 * SPI
 **/
void DEV_SPI_WriteByte(uint8_t Value) { ssp1__exchange_byte(Value); }

void DEV_SPI_Write_nByte(uint8_t *pData, uint32_t Len) {
  for (int i = 0; i < Len; i++) {
    ssp1__exchange_byte(pData[i]);
  }
}

/**
 * GPIO Mode
 **/
// using SSP1
void DEV_GPIO_Mode(void) {
  LPC_IOCON->P0_7 &= ~0b111;  // SCK1
  LPC_IOCON->P0_9 &= ~0b111;  // MOSI1
  LPC_IOCON->P0_8 &= ~0b111;  // MISO1
  LPC_IOCON->P1_14 &= ~0b111; // CS

  LPC_IOCON->P0_7 |= (0b1 << 1);
  LPC_IOCON->P0_8 |= (0b1 << 1);
  LPC_IOCON->P0_9 |= (0b1 << 1);
  // | P1.31 RST | P1.30 DC |P1.20 BUSY
  // Initialize CS as output
  gpio0__set_as_output(1, lcd_pin_init.EPD_CS_PIN);
  gpio0__set_as_input(1, lcd_pin_init.EPD_BUSY_PIN);
  gpio0__set_as_output(1, lcd_pin_init.EPD_DC_PIN);
  gpio0__set_as_output(1, lcd_pin_init.EPD_RST_PIN);
}

/**
 * delay x ms
 **/
void DEV_Delay_ms(UDOUBLE xms) { delay__ms(xms); }

void DEV_GPIO_Init(void) {

  DEV_GPIO_Mode();
  // Active Low CS
  DEV_Digital_Write(lcd_pin_init.EPD_CS_PIN, 1);
}
/******************************************************************************
function:	Module Initialize, the library and initialize the pins, SPI protocol
parameter:
Info:
******************************************************************************/
UBYTE DEV_Module_Init(void) {
  printf("/***********************************/ \r\n");

  // GPIO Config
  DEV_GPIO_Init();

  const UDOUBLE spi_clock_mhz = 1;
  ssp1__initialize(spi_clock_mhz);

  printf("/***********************************/ \r\n");
  return 0;
}

/******************************************************************************
function:	Module exits, closes SPI and BCM2835 library
parameter:
Info:
******************************************************************************/
void DEV_Module_Exit(void) {
  DEV_Digital_Write(lcd_pin_init.EPD_CS_PIN, 0);

  // DEV_Digital_Write(EPD_RST_PIN, 1);
  // DEV_Digital_Write(EPD_DC_PIN, 1);
  // DEV_Digital_Write(EPD_BUSY_PIN, 1);
}