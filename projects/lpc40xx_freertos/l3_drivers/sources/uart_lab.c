#include "uart_lab.h"
#include "FreeRTOS.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "queue.h"
#include <stdio.h>
// UART3 P0.0-TX3  P0.1-RX3
// UART2 P0.10-TX2 P0.11-RX2
// Private queue handle of our uart_lab.c
static QueueHandle_t your_uart_rx_queue;
void uart_lab__init(uart_number_e uart, uint32_t peripheral_clock, uint32_t baud_rate) {
  // Refer to LPC User manual and setup the register bits correctly
  // The first page of the UART chapter has good instructions
  // a) Power on Peripheral
  // b) Setup DLL, DLM, FDR, LCR registers
  if (uart == UART_3) {
    LPC_SC->PCONP |= (1 << 25);
    uint16_t dll_value = (uint16_t)(peripheral_clock / (16 * baud_rate));
    LPC_UART3->LCR |= (1 << 7); // enable DLAB to set DLM DLL
    LPC_UART3->DLL = (dll_value >> 0) & 0xFF;
    LPC_UART3->DLM = (dll_value >> 8) & 0xFF;
    LPC_UART3->LCR |= (0x3 << 0) | (1 << 2); // frame 8bit and 2 bit END
    // Set FDR to reset ;
    // LPC_UART3->FDR &= ~(0xF << 0);
    // LPC_UART3->FDR &= ~(0XF << 4);
    // LPC_UART3->FDR |= (1 << 4);
    LPC_UART3->LCR &= ~(1 << 7); // Disable DLAB for THR available
  } else if (uart == UART_2) {
    LPC_SC->PCONP |= (1 << 24);
    uint16_t dll_value = (uint16_t)(peripheral_clock / (16 * baud_rate));
    LPC_UART2->LCR |= (1 << 7); // enable DLAB to set DLM DLL
    LPC_UART2->DLL = (dll_value >> 0) & 0xFF;
    LPC_UART2->DLM = (dll_value >> 8) & 0xFF;
    LPC_UART2->LCR |= (0x3 << 0) | (1 << 2); // frame 8bit and 2 bit END
    // Set FDR to reset ;
    LPC_UART2->FDR &= ~(0xF << 0);
    LPC_UART2->FDR &= ~(0XF << 4);
    LPC_UART2->FDR |= (1 << 4);
    LPC_UART2->LCR &= ~(1 << 7); // Disable DLAB for THR available
  }
}

bool uart_lab__polled_get(uart_number_e uart, char *input_byte) {
  bool status = false;
  if (uart == UART_3) {
    while (!(LPC_UART3->LSR & (1 << 0))) {
    }
    *input_byte = LPC_UART3->RBR;
    status = true;
  } else if (uart == UART_2) {
    while (!(LPC_UART2->LSR & (1 << 0))) {
    }
    *input_byte = LPC_UART2->RBR;
    status = true;
  } else
    status = false;
  return status;
}

// a) Check LSR for Transmit Hold Register Empty
// b) Copy output_byte to THR register
bool uart_lab__polled_put(uart_number_e uart, char output_byte) {
  bool status;
  if (uart == UART_2) {
    while (!(LPC_UART2->LSR & (1 << 5))) {
    }
    LPC_UART2->THR = output_byte;
    while (!(LPC_UART2->LSR & (1 << 5))) {
    }
    status = true;
  } else if (uart == UART_3) {
    while (!(LPC_UART3->LSR & (1 << 5))) {
    }
    LPC_UART3->THR = output_byte;
    while (!(LPC_UART3->LSR & (1 << 5))) {
    }
    status = true;
  } else
    status = false;
  return status;
}

// Private function of our uart_lab.c
static void your_receive_interrupt(void) {
  // TODO: Read the IIR register to figure out why you got interrupted

  // TODO: Based on IIR status, read the LSR register to confirm if there is data to be read
  if (!(LPC_UART3->IIR & (1 << 0))) {
    while (!(LPC_UART3->LSR & (1 << 0))) {
    }
  }
  // TODO: Based on LSR status, read the RBR register and input the data to the RX Queue
  const char byte = LPC_UART3->RBR;
  xQueueSendFromISR(your_uart_rx_queue, &byte, NULL);
}

// Public function to enable UART interrupt
// TODO Declare this at the header file
void uart__enable_receive_interrupt(uart_number_e uart_number) {
  // TODO: Use lpc_peripherals.h to attach your interrupt
  if (uart_number == UART_3)
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART3, your_receive_interrupt);
  if (uart_number == UART_2)
    lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__UART2, your_receive_interrupt);

  // TODO: Enable UART receive interrupt by reading the LPC User manual
  // Hint: Read about the IER register
  if (uart_number == UART_3) {
    LPC_UART3->IER |= (1 << 0);
  }
  // TODO: Create your RX queue
  your_uart_rx_queue = xQueueCreate(16, sizeof(char));
}

// Public function to get a char from the queue (this function should work without modification)
// TODO: Declare this at the header file
bool uart_lab__get_char_from_queue(char *input_byte, uint32_t timeout) {
  return xQueueReceive(your_uart_rx_queue, input_byte, timeout);
}