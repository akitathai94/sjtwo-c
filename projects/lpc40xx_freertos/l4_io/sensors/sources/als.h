#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct {
  uint16_t clear_light_data;
} ambient_light_sensor_data_s;

bool als__init(void);

uint16_t als__get_data(void);
