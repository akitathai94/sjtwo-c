#include "mp3_module.h"
const mp3_pin_setup mp3_pin_init = {2, 0, 2, 5};
mp3_config_t mp3_config = {false, VOL_DEFAULT, 6, false, 0, false, false, false};
void set_CS() { gpio0__set_low(mp3_pin_init.port, mp3_pin_init.cs_pin); }
void reset_CS() { gpio0__set_high(mp3_pin_init.port, mp3_pin_init.cs_pin); }
void set_xDCS() { gpio0__set_low(mp3_pin_init.port, mp3_pin_init.dcs_pin); }
void reset_xDCS() { gpio0__set_high(mp3_pin_init.port, mp3_pin_init.dcs_pin); }

void write_register(uint8_t addr, uint16_t data) {

  reset_CS();
  set_CS();
  ssp0__exchange_byte(0x02); // opcode to write
  ssp0__exchange_byte(addr);
  ssp0__exchange_byte((data >> 8) & 0xFF);
  ssp0__exchange_byte(data & 0xFF);
  data_line_request();
  reset_CS();
}

uint16_t read_register(uint8_t addr) {
  uint16_t data;
  set_CS();
  ssp0__exchange_byte(0x03); // opcode to read
  ssp0__exchange_byte(addr);
  char firstByte = ssp0__exchange_byte(0xFF);
  data_line_request();
  char secondByte = ssp0__exchange_byte(0xFF);
  data_line_request();
  reset_CS();
  data = (firstByte << 8);
  data = data | secondByte;
  return data;
}

// check status of DREQ register through
bool mp3_decoder_needs_data() {
  bool dreq = true;
  // write_register(0x7, 0xC012); // write 0xC012 to address 0x7
  // dreq = read_register(0x6) & 1;
  if (gpio0__get_level(mp3_pin_init.port, mp3_pin_init.dreq_pin)) {
    dreq = true;
  } else
    dreq = false;
  return dreq;
}

void mp3_init() {
  // Initialize pin for SSP0 MP3 decoder
  LPC_IOCON->P0_15 &= ~(0b111 << 0);
  LPC_IOCON->P0_17 &= ~(0b111 << 0);
  LPC_IOCON->P0_18 &= ~(0b111 << 0);
  LPC_IOCON->P0_15 |= (0b1 << 1);
  LPC_IOCON->P0_17 |= (0b1 << 1);
  LPC_IOCON->P0_18 |= (0b1 << 1);
  // Initialize Speed for Decoder
  ssp0__initialize(5000);
  // Set up pin for input output
  gpio0__set_as_input(mp3_pin_init.port, mp3_pin_init.dreq_pin);
  gpio0__set_as_output(mp3_pin_init.port, mp3_pin_init.cs_pin);
  gpio0__set_as_output(mp3_pin_init.port, mp3_pin_init.dcs_pin);
  gpio0__set_high(mp3_pin_init.port, mp3_pin_init.dcs_pin);
  gpio0__set_high(mp3_pin_init.port, mp3_pin_init.cs_pin);
  write_register(SCI_MODE, 0x0800);
  write_register(SCI_BASS, 0x7A00);
  write_register(SCI_CLOCKF, 0x2000);
  write_register(SCI_AUDATA, 0xAC45);
  write_register(SCI_VOL, mp3_config.volume);
  puts("Initialized mp3 module\n");
}

void spi_send_to_mp3_decoder(uint8_t data) {
  reset_CS();
  set_xDCS();
  ssp0__exchange_byte(data);
  reset_xDCS();
}

void softReset() {
  puts("Performing soft-reset\n");
  write_register(0x0, (11 << 1) | (2 << 1));
  vTaskDelay(10);
}

// Set volume for MP3 decoder
void setVolume(uint8_t level) { write_register(SCI_VOL, (level << 8 | level)); }

// get current volume
uint16_t getCurrentVolume() { return mp3_config.volumeCurrent; }
// Set volume up
void volumeUp() {
  data_line_request();
  if (mp3_config.volumeCurrent < 11) {
    if (mp3_config.mute == true) {
      mp3_config.mute = false;
      mp3_config.volume = 0xAAAA;
    }
    mp3_config.volume -= 0x0f0f;
    mp3_config.volumeCurrent += 1;
  }
  setVolume(mp3_config.volume);
}
// Set volume down
void volumeDown() {
  data_line_request();
  if ((mp3_config.volumeCurrent > 1) && (mp3_config.volumeCurrent <= 11) && (mp3_config.mute != true)) {
    mp3_config.mute = false;
    mp3_config.volume += 0x0f0f;
    mp3_config.volumeCurrent -= 1;
  }
  setVolume(mp3_config.volume);
}
void data_line_request() {
  while (!mp3_decoder_needs_data()) {
    // puts("Inside data request");
  }
}
