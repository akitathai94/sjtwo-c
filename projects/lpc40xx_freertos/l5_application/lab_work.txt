#include "FreeRTOS.h"
#include "adc.h"
#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "gpio_isr.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "periodic_scheduler.h"
#include "pwm1.h"
#include "queue.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "task.h"
#include <stdio.h>
#include <ssp2.h>

#if 0
static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);
static void task_one(void *task_parameters);
static void task_two(void *task_parameters);

void led_task(void *pvParameters);

typedef __attribute__((packed)) struct {
  float f1; // 4 bytes
  char c1;  // 1 bytes
  float f2; // 4 bytes
  char c2;  // 1 bytes
} my_struct_t;

int main(void) {
  struct homework
  my_struct_t s;
  struct_as_pointer(&s);
  printf("Size of the struct %d\n", sizeof(s));
  printf("Base: %p\n"
         "floats %p %p\n"
         "chars %p %p\n",
         &s, &s.f1, &s.f2, &s.c1, &s.c2);
  puts("Starting RTOS");
  //(); // This function never returns unless RTOS scheduler runs out of memory and fails
  xTaskCreate(task_one, "task1", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(task_two, "task2", 4096 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  Create FreeRTOS LED task
   xTaskCreate(led_task, "led", 1024 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
  return 0;
}
// Pass data structure as a copy
void struct_as_param(my_struct_t s) {
  s.f1 = 0;
  s.c1 = 'c';
  s.f2 = 0;
  s.c2 = 'c';
}
// Pass data structure as a pointer
void struct_as_pointer(my_struct_t *p) {
  p->f1 = 0;
  p->c1 = 'c';
  p->f2 = 0;
  p->c2 = 'c';
}

static void task_one(void *task_parameters) {
  while (true) {
    fprintf(stderr, "AAAAAAAAAAAA");
    vTaskDelay(100);
  }
}

static void task_two(void *task_parameters) {
  while (true) {
    fprintf(stderr, "bbbbbbbbbbbb");
    vTaskDelay(100);
  }
}

void led_task(void *pvParameters) {
  //  0) Set the IOCON MUX function select pins to 000
  // 1) Set the DIR register bit for the LED port pin1.18
  LPC_GPIO1->DIR |= (1 << 18);
  while (1) {
    // 2) User CLR register to turn the LED on (led may be active low)
    LPC_GPIO1->CLR = (1 << 18);
    vTaskDelay(500);
    // 3) Use SET register to turn the LED off
    LPC_GPIO1->SET = (1 << 18);
    vTaskDelay(500);
  }
}
static void create_blinky_tasks(void) {
  /**
   * Use '#if (1)' if you wish to observe how two tasks can blink LEDs
   * Use '#if (0)' if you wish to use the 'periodic_scheduler.h' that will spawn 4 periodic tasks, one for each LED
   */
#if (1)
  // These variables should not go out of scope because the 'blink_task' will reference this memory
  static gpio_s led0, led1;

  led0 = board_io__get_led0();
  led1 = board_io__get_led1();

  xTaskCreate(blink_task, "led0", configMINIMAL_STACK_SIZE, (void *)&led0, PRIORITY_LOW, NULL);
  xTaskCreate(blink_task, "led1", configMINIMAL_STACK_SIZE, (void *)&led1, PRIORITY_LOW, NULL);
#else
  const size_t stack_size_bytes = 2048 / sizeof(void *);
  periodic_scheduler__initialize(stack_size_bytes);
  UNUSED(blink_task);
#endif
}

static void create_uart_task(void) {
  // It is advised to either run the uart_task, or the SJ2 command-line (CLI), but not both
  // Change '#if (0)' to '#if (1)' and vice versa to try it out
#if (0)
  // printf() takes more stack space, size this tasks' stack higher
  xTaskCreate(uart_task, "uart", (512U * 8) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#else
  sj2_cli__init();
  UNUSED(uart_task); // uart_task is un-used in if we are doing cli init()
#endif
}

static void blink_task(void *params) {
  const gpio_s led = *((gpio_s *)params); // Parameter was input while calling xTaskCreate()

  // Warning: This task starts with very minimal stack, so do not use printf() API here to avoid stack overflow
  while (true) {
    gpio__toggle(led);
    vTaskDelay(500);
  }
}

// This sends periodic messages over printf() which uses system_calls.c to send them to UART0
static void uart_task(void *params) {
  TickType_t previous_tick = 0;
  TickType_t ticks = 0;

  while (true) {
    // This loop will repeat at precise task delay, even if the logic below takes variable amount of ticks
    vTaskDelayUntil(&previous_tick, 2000);

    /* Calls to fprintf(stderr, ...) uses polled UART driver, so this entire output will be fully
     * sent out before this function returns. See system_calls.c for actual implementation.
     *
     * Use this style print for:
     *  - Interrupts because you cannot use printf() inside an ISR
     *    This is because regular printf() leads down to xQueueSend() that might block
     *    but you cannot block inside an ISR hence the system might crash
     *  - During debugging in case system crashes before all output of printf() is sent
     */
    ticks = xTaskGetTickCount();
    fprintf(stderr, "%u: This is a polled version of printf used for debugging ... finished in", (unsigned)ticks);
    fprintf(stderr, " %lu ticks\n", (xTaskGetTickCount() - ticks));

    /* This deposits data to an outgoing queue and doesn't block the CPU
     * Data will be sent later, but this function would return earlier
     */
    ticks = xTaskGetTickCount();
    printf("This is a more efficient printf ... finished in");
    printf(" %lu ticks\n\n", (xTaskGetTickCount() - ticks));
  }
}
typedef struct {
  uint8_t port;
  uint8_t pin;
} port_pin_s;
static SemaphoreHandle_t switch_press_indication;
void led_task(void *task_parameter);    // Part 1
void switch_task(void *task_parameter); // Switch control
void led_switch(void *task_parameter);  // Part 3
void led_flashy(void *task_parameter);  // Extra credit

int main(void) {
  switch_press_indication = xSemaphoreCreateBinary();
  // TODO:
  // Pass each task its own parameter:
  // This is static such that these variables will be allocated in RAM and not go out of scope
  static port_pin_s switch_led = {2, 8};
  static port_pin_s led_external = {1, 20};
  static port_pin_s led_0 = {2, 3};
  static port_pin_s led_1 = {1, 26};
  static port_pin_s led_2 = {1, 24};
  static port_pin_s led_3 = {1, 18};
  static port_pin_s led_seq[4] = {{2, 3}, {1, 26}, {1, 24}, {1, 18}};
  // Task 1
  // xTaskCreate(led_task, "switch_task", 2048 / sizeof(void *), (void *)&led_0, PRIORITY_LOW, NULL);
  // xTaskCreate(led_task, "switch_task", 2048 / sizeof(void *), (void *)&led_1, PRIORITY_LOW, NULL);
  // Task 3
  xTaskCreate(switch_task, "switch_task", 2048 / sizeof(void *), (void *)&switch_led, PRIORITY_LOW, NULL);
  xTaskCreate(led_switch, "led_task", 2048 / sizeof(void *), (void *)&led_external, PRIORITY_LOW, NULL);
  // Extra credit:
  // xTaskCreate(switch_task, "switch_task", 2048 / sizeof(void *), (void *)&switch_led, PRIORITY_LOW, NULL);
  // xTaskCreate(led_flashy, "led_flash", 2048 / sizeof(void *), led_seq, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
  return 0;
}
void switch_task(void *task_parameter) {
  port_pin_s *switch_led = (port_pin_s *)(task_parameter);
  while (true) {
    gpio0__set_as_input(switch_led->port, switch_led->pin);
    // TODO: If switch pressed, set the binary semaphore
    if (!gpio0__get_level(switch_led->port, switch_led->pin)) {
      puts("Button pressed");
      xSemaphoreGive(switch_press_indication);
    }
    // Task should always sleep otherwise they will use 100% CPU
    // This task sleep also helps avoid spurious semaphore give during switch debeounce
    vTaskDelay(100);
  }
}
// Part 3
void led_switch(void *task_parameter) {
  // Type-cast the paramter that was passed from xTaskCreate()
  port_pin_s *led = (port_pin_s *)(task_parameter);
  while (true) {
    // Note: There is no vTaskDelay() here, but we use sleep mechanism while waiting for the binary semaphore
    if (xSemaphoreTake(switch_press_indication, 1000)) {
      // TODO: Blink the LED
      gpio0__set_as_output(led->port, led->pin);
      gpio0__set_high(led->port, led->pin);
      vTaskDelay(500);
      gpio0__set_low(led->port, led->pin);
      vTaskDelay(500);
    } else {
      puts("Timeout: No switch press indication for 1000ms");
    }
  }
}
void led_flashy(void *task_parameter) {
  port_pin_s *led_flash_ptr = (port_pin_s *)(task_parameter);
  while (true) {
    if (xSemaphoreTake(switch_press_indication, 1000)) {
      for (int i = 0; i <= 3; i++) {
        gpio0__set_as_output(led_flash_ptr[i].port, led_flash_ptr[i].pin);
        gpio0__set_high(led_flash_ptr[i].port, led_flash_ptr[i].pin);
        vTaskDelay(300);
        gpio0__set_low(led_flash_ptr[i].port, led_flash_ptr[i].pin);
        vTaskDelay(300);
      }
    } else {
      puts("Timeout: No switch press indication for 1000ms");
    }
  }
}
void led_task(void *task_parameter) {
  port_pin_s *led = (port_pin_s *)(task_parameter);
  while (1) {
    gpio0__set_as_output(led->port, led->pin);
    gpio0__set_high(led->port, led->pin);
    vTaskDelay(500);
    gpio0__set_low(led->port, led->pin);
    vTaskDelay(500);
  }
}
#endif
#if 0
// PART 0 of Lab Interrupt
// Step 1:
typedef struct {
  uint8_t port;
  uint8_t pin;
} port_pin_s;

void gpio_interrupt(void);

void main(void) {
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio_interrupt);
  gpio0__set_as_input(0, 29);
  LPC_GPIOINT->IO0IntEnR |= (1 << 29);
  NVIC_EnableIRQ(GPIO_IRQn);

  while (1) {
    delay__ms(100);
    // TODO: Toggle an LED here
    fprintf(stderr, "Inside Main");
    gpio0__set_as_output(1, 26);
    gpio0__set_high(1, 26);
    delay__ms(500);
    gpio0__set_low(1, 26);
    delay__ms(500);
    ;
  }
}

// Step 2:
void gpio_interrupt(void) {
  // a) Clear Port0/2 interrupt using CLR0 or CLR2 registers
  LPC_GPIOINT->IO0IntClr |= (1 << 29);
  // b) Use fprintf(stderr) or blink and LED here to test your ISR
  fprintf(stderr, "Inside the interrupt\n");
  vTaskDelay(200);
}
#endif
#if 0
// Part 1 of Lab Interrupt
typedef struct {
  uint8_t port;
  uint8_t pin;
} port_pin_s;
static SemaphoreHandle_t switch_pressed_signal;
void gpio_interrupt(void);
void sleep_on_sem_task(void *p);
void main(void) {
  static port_pin_s led = {1, 26};
  switch_pressed_signal = xSemaphoreCreateBinary();
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio_interrupt);
  gpio0__set_as_input(0, 29);
  LPC_GPIOINT->IO0IntEnR |= (1 << 29);
  NVIC_EnableIRQ(GPIO_IRQn);
  xTaskCreate(sleep_on_sem_task, "sem", (512U * 4) / sizeof(void *), &led, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
}

void gpio_interrupt(void) {
  xSemaphoreGiveFromISR(switch_pressed_signal, NULL);
  // a) Clear Port0/2 interrupt using CLR0 or CLR2 registers
  LPC_GPIOINT->IO0IntClr |= (1 << 29);
  // b) Use fprintf(stderr) or blink and LED here to test your ISR
  fprintf(stderr, "Inside the interrupt\n");
}

void sleep_on_sem_task(void *p) {
  port_pin_s *led = (port_pin_s *)(p);
  while (1) {
    if (xSemaphoreTake(switch_pressed_signal, portMAX_DELAY)) {
      fprintf(stderr, "Inside Sleeping Task\n");
      gpio0__set_as_output(led->port, led->pin);
      gpio0__set_high(led->port, led->pin);
      vTaskDelay(500);
      gpio0__set_low(led->port, led->pin);
      vTaskDelay(500);
    } else
      puts("Waiting on signal\n");
  }
}
#endif

// Part 2:
typedef struct {
  uint8_t port;
  uint8_t pin;
} port_pin_s;
// void pin29_isr(void);
// void pin30_isr(void);

// static SemaphoreHandle_t switch_pressed_signal;
// static QueueHandle_t adc_to_pwm_task_queue;
// void gpio_interrupt(void);
// void sleep_on_sem_task(void *p);
// void pwm_task(pwm1_channel_e);
// void adc_task(void *P);

void main(void) {
  // static port_pin_s led = {1, 26};
  // switch_pressed_signal = xSemaphoreCreateBinary();
  // lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio0__interrupt_dispatcher);
  // NVIC_EnableIRQ(GPIO_IRQn);
  // gpio0__set_as_input(0, 29);
  // gpio0__set_as_input(0, 30);
  // gpio0__attach_interrupt(30, GPIO_INTR__RISING_EDGE, pin29_isr);
  // gpio0__attach_interrupt(29, GPIO_INTR__FALLING_EDGE, pin30_isr);
  // LPC_IOCON->P0_30 &= ~(3 << 3);
  // LPC_IOCON->P0_30 |= (1 << 3);
  // xTaskCreate(sleep_on_sem_task, "sem", (512U * 4) / sizeof(void *), &led, PRIORITY_LOW, NULL);
  //adc_to_pwm_task_queue = xQueueCreate(1, sizeof(int));
  // Part 0:
  // xTaskCreate(pwm_task, "pwm1", (4096) / sizeof(void *), PWM1__2_0, PRIORITY_LOW, NULL);
  // xTaskCreate(pwm_task, "pwm2", (2048) / sizeof(void *), PWM1__2_1, PRIORITY_LOW, NULL);
  // xTaskCreate(pwm_task, "pwm3", (2048) / sizeof(void *), PWM1__2_2, PRIORITY_LOW, NULL);
  // // P2_0, ADC_CHANNEL 2;
  // xTaskCreate(adc_task, "adc1", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // vTaskStartScheduler();
}

// void gpio_interrupt(void) {
//   xSemaphoreGiveFromISR(switch_pressed_signal, NULL);
//   // a) Clear Port0/2 interrupt using CLR0 or CLR2 registers
//   LPC_GPIOINT->IO0IntClr |= (1 << 29);
//   // b) Use fprintf(stderr) or blink and LED here to test your ISR
//   fprintf(stderr, "Inside the interrupt\n");
// }

// void sleep_on_sem_task(void *p) {
//   port_pin_s *led = (port_pin_s *)(p);
//   while (1) {
//     if (xSemaphoreTake(switch_pressed_signal, portMAX_DELAY)) {
//       fprintf(stderr, "Inside Sleeping Task\n");
//       gpio0__set_as_output(led->port, led->pin);
//       gpio0__set_high(led->port, led->pin);
//       vTaskDelay(500);
//       gpio0__set_low(led->port, led->pin);
//       vTaskDelay(500);
//     } else
//       puts("Waiting on signal\n");
//   }
// }

// void pin29_isr(void) {
//   xSemaphoreGiveFromISR(switch_pressed_signal, NULL);
//   fprintf(stderr, "Inside ISR pin29\n");
// }
// void pin30_isr(void) {
//   xSemaphoreGiveFromISR(switch_pressed_signal, NULL);
//   fprintf(stderr, "Inside ISR pin30\n");
// }

// void pwm_task(pwm1_channel_e pwm) {
//   uint32_t result_MR0 = 0;
//   uint32_t result_MR1 = 0;
//   pwm1__init_single_edge(1000);
//   // Locate a GPIO pin that a PWM channel will control
//   // NOTE You can use gpio__construct_with_function() API from gpio.h
//   // TODO Write this function yourself
//   pin_configure_pwm_channel_as_io_pin(pwm);
//   // We only need to set PWM configuration once, and the HW will drive
//   // the GPIO at 1000Hz, and control set its duty cycle to 50%
//   pwm1__set_duty_cycle(pwm, 50);
//   // Continue to vary the duty cycle in the loop
//   int adc_reading = 0;
//   float percent = 0;
//   float adc_voltage = 0;
//   while (1) {
//     // Implement code to receive potentiometer value from queue
//     if (xQueueReceive(adc_to_pwm_task_queue, &adc_reading, 100)) {
//       printf("MR0 %d MR1 %d \n", result_MR0, result_MR1);
//       result_MR0 = LPC_PWM1->MR0;
//       result_MR1 = LPC_PWM1->MR1;
//       adc_voltage = ((float)adc_reading * 3.3) / 4095;
//       percent = (adc_voltage / 3.3) * 100;
//       printf("Voltage: %4.2f V\n", adc_voltage);
//       printf("Percent: %4.2f \n", percent);
//       pwm1__set_duty_cycle(pwm, percent);
//       puts("Received data from queue\n");
//     }
//     // We do not need task delay because our queue API will put task to sleep when there is no data in the queue
//     // vTaskDelay(100);
//   }
// }

// void adc_task(void *p) {
//   adc__initialize();

//   // This is the function you need to add to adc.h
//   // You can configure burst mode for just the channel you are using
//   adc__enable_burst_mode();

//   // Configure a pin, such as P0.25 with FUNC 001 to route this pin as ADC channel 2
//   // You can use gpio__construct_with_function() API from gpio.h
//   LPC_IOCON->P0_25 &= ~(3 << 3);
//   LPC_IOCON->P0_25 |= (0b001 << 0); // You need to write this function
//   LPC_IOCON->P0_25 &= ~(1 << 7);    // Enable analog input signal
//   gpio__construct_as_input(GPIO__PORT_0, 25);
//   int adc_reading = 0;
//   while (1) {
//     adc_reading = adc__get_channel_reading_with_burst_mode(ADC__CHANNEL_2);
//     printf("ADC value : %d\n", adc_reading);
//     if (xQueueSend(adc_to_pwm_task_queue, &adc_reading, 0)) {
//       puts("Data send queue:\n");
//     } else {
//       puts("Failed to send\n");
//     }
//     vTaskDelay(100);
//   }
// }


// Lab SPI 

// TODO: Implement Adesto flash memory CS signal as a GPIO driver
void adesto_cs(void);
void adesto_ds(void);

// TODO: Study the Adesto flash 'Manufacturer and Device ID' section
typedef struct {
  uint8_t manufacturer_id;
  uint8_t device_id_1;
  uint8_t device_id_2;
  uint8_t extended_device_id;
} adesto_flash_id_s;

adesto_flash_id_s adesto_read_signature(void);
static SemaphoreHandle_t spi_bus_mutex;
void spi_task(void *p);
void spi_id_verification_task(void *p);

void main(void) {
  const uint32_t spi_clock_mhz = 1;
  ssp2__init_rw(spi_clock_mhz);
  spi_bus_mutex = xSemaphoreCreateMutex();
  todo_configure_your_ssp2_pin_functions();

  xTaskCreate(spi_id_verification_task, "ssp2_t1", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(spi_id_verification_task, "ssp2_t2", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // xTaskCreate(spi_task, "ssp2", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  vTaskStartScheduler();
}

void todo_configure_your_ssp2_pin_functions() {
  // P1_0 = SCK2 P1_1 = MOSI2 P1_10 = CS_GPIO  MISO = P1_4
  LPC_IOCON->P1_0 |= (0b100 << 0);
  LPC_IOCON->P1_1 |= (0b100 << 0);
  LPC_IOCON->P1_4 |= (0b100 << 0);
  gpio__construct_as_output(GPIO__PORT_1, 10);
  gpio__construct_as_output(GPIO__PORT_2, 0);
  gpio0__set_high(2, 0);
  gpio0__set_high(1, 10);
}

void adesto_cs(void) {
  gpio0__set_low(1, 10);
  gpio0__set_low(2, 0);
  puts("Set low");
}
void adesto_ds(void) {
  gpio0__set_high(1, 10);
  gpio0__set_high(2, 0);
  puts("Set high");
}
adesto_flash_id_s adesto_read_signature(void) {
  adesto_flash_id_s data = {0};

  adesto_cs();
  // Send opcode and read bytes
  ssp2__exchange_byte_rw(0x9F);
  // TODO: Populate members of the 'adesto_flash_id_s' struct
  data.manufacturer_id = ssp2__exchange_byte_rw(0xFF);
  data.device_id_1 = ssp2__exchange_byte_rw(0xFF);
  data.device_id_2 = ssp2__exchange_byte_rw(0xFF);
  data.extended_device_id = ssp2__exchange_byte_rw(0xFF);
  adesto_ds();
  return data;
}

void spi_task(void *p) {
  const uint32_t spi_clock_mhz = 24;
  ssp2__init_rw(spi_clock_mhz);

  // From the LPC schematics pdf, find the pin numbers connected to flash memory
  // Read table 84 from LPC User Manual and configure PIN functions for SPI2 pins
  // You can use gpio__construct_with_function() API from gpio.h
  //
  // Note: Configure only SCK2, MOSI2, MISO2.
  // CS will be a GPIO output pin(configure and setup direction)
  todo_configure_your_ssp2_pin_functions();

  while (1) {
    adesto_flash_id_s id = adesto_read_signature();
    // TODO: printf the members of the 'adesto_flash_id_s' struct
    printf("Manufacture ID : %p\n", id.manufacturer_id);
    printf("Device id 1 : %p\n", id.device_id_1);
    printf("Device id 2 : %p\n", id.device_id_2);
    printf("Extended device id : %p\n", id.extended_device_id);
    vTaskDelay(500);
  }
}

void spi_id_verification_task(void *p) {
  while (1) {
    if (xSemaphoreTake(spi_bus_mutex, 1000)) {
      // Use Guarded Resource
      const adesto_flash_id_s id = adesto_read_signature();
      // Give Semaphore back
      printf("Manufacture ID : %X\n", id.manufacturer_id);
      printf("Device id 1 : %X\n", id.device_id_1);
      printf("Device id 2 : %X\n", id.device_id_2);
      printf("Extended device id : %X\n", id.extended_device_id);
      vTaskDelay(500);
      xSemaphoreGive(spi_bus_mutex);

      // When we read a manufacturer ID we do not expect, we will kill this task
      if (id.manufacturer_id != 0x1F) {
        fprintf(stderr, "Manufacturer ID read failure\n");
        vTaskSuspend(NULL); // Kill this task
      }
    }
  }
}


// Lab UART 
void uart_write_task(void *p);
void uart_read_task(void *p);
void board_2_receiver_task(void *p);
void board_1_sender_task(void *p);
void board_2_sender_task(void *p);
void main(void) {
  // TODO: Pin Configure IO pins to perform UART2/UART3 function
  LPC_IOCON->P0_0 &= ~(0b111 << 0);
  LPC_IOCON->P0_1 &= ~(0b111 << 0);
  LPC_IOCON->P0_10 &= ~(0b111 << 0);
  LPC_IOCON->P0_11 &= ~(0b111 << 0);

  LPC_IOCON->P0_0 |= (1 << 1);     // TX3
  LPC_IOCON->P0_1 |= (1 << 1);     // Rx3
  LPC_IOCON->P0_1 &= ~(0b11 << 3); // Disable pull from receiver
  // TODO: Use uart_lab__init() function and initialize UART2 or UART3 (your choice)
  LPC_IOCON->P0_10 |= (1 << 0); // TX2
  LPC_IOCON->P0_11 |= (1 << 0); // RX2
  const uint32_t peripheral_clock = clock__get_peripheral_clock_hz();
  const uint32_t baud_rate = (UINT32_C(1152) * 100);
  uart_lab__init(UART_3, peripheral_clock, baud_rate);
  uart__enable_receive_interrupt(UART_3);
  uart_lab__init(UART_2, peripheral_clock, baud_rate);
  xTaskCreate(board_2_sender_task, "uart2_write", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(board_1_sender_task, "uart3_write", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();
}

void board_1_sender_task(void *p) {
  char number_as_string[16] = {0};
  vTaskDelay(3000);
  while (true) {
    const int number = rand();
    sprintf(number_as_string, "%i", number);

    // Send one char at a time to the other board including terminating NULL char
    for (int i = 0; i <= strlen(number_as_string); i++) {
      uart_lab__polled_put(UART_3, number_as_string[i]);
      // printf("Sent: %c\n", number_as_string[i]);
    }

    // printf("Sent: %i over UART to the other board\n", number);
    vTaskDelay(3000);
  }
}
void board_2_sender_task(void *p) {
  char number_as_string[16] = {0};

  while (true) {
    const int number = rand();
    sprintf(number_as_string, "%i", number);

    // Send one char at a time to the other board including terminating NULL char
    for (int i = 0; i <= strlen(number_as_string); i++) {
      uart_lab__polled_put(UART_2, number_as_string[i]);
      // printf("Sent: %c\n", number_as_string[i]);
    }

    // printf("Sent: %i over UART to the other board\n", number);
    vTaskDelay(3000);
  }
}
void board_2_receiver_task(void *p) {
  char number_as_string[16] = {0};
  int counter = 0;

  while (true) {
    char byte = 0;
    uart_lab__get_char_from_queue(&byte, portMAX_DELAY);
    printf("Received: %c\n", byte);

    // This is the last char, so print the number
    if ('\0' == byte) {
      number_as_string[counter] = '\0';
      counter = 0;
      printf("Received this number from the other board: %s\n", number_as_string);
    }
    // We have not yet received the NULL '\0' char, so buffer the data
    else {
      // TODO: Store data to number_as_string[] array one char at a time
      // Hint: Use counter as an index, and increment it as long as we do not reach max value of 16
      if (counter < 16) {
        number_as_string[counter] = byte;
        counter = counter + 1;
      }
    }
  }
}

void uart_read_task(void *p) {
  char *value = NULL;
  while (1) {
    // TODO: Use uart_lab__polled_get() function and printf the received value
    uart_lab__polled_get(UART_3, value);
    printf("Value received is %c \n", value);
    vTaskDelay(500);
  }
}

void uart_write_task(void *p) {
  char data = 'T';
  while (1) {
    // TODO: Use uart_lab__polled_put() function and send a value
    uart_lab__polled_put(UART_3, data);
    vTaskDelay(500);
  }
}

// Lab Producer Consumer Task

#include "FreeRTOS.h"
#include "adc.h"
#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "gpio_isr.h"
#include "gpio_lab.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "periodic_scheduler.h"
#include "pwm1.h"
#include "queue.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "task.h"
#include "uart_lab.h"
#include <ssp2.h>
#include <stdio.h>
#include <string.h>
#include <uart.h>

/*
producer high - consumer low:
producer(), line 47, Before sending to queue
produmer(), line 62, Before rece
consumer(), line 62, Before receive value from queue
consumer(), line 65, Received value is 0
consumer(), line 62, Before receive value from queue
producer(), line 47, Before sending to queue
Producer task start first send item to a queue and sleep context switch to a consumer task to get the value 

producer low - consumer high
consumer(), line 69, Before receive value from queue
producer(), line 54, Before sending to queue
consumer(), line 58, Queue sent
lue is 0
consumer(), line 69, Before receive value from queue
ponsumer(), line 69, Before receproducer(), line 54, Before sending to queue
consucer(), line 58, Queue sent
lue is 0
Consumer start first as a higher priority but it is empty queue so it sleeps and context switch to producer 
and finish sending queue and context switch back to producer to get the item from the queue

producer low - consumer low
consumer(), line 77, Before receive value from queue
producer(), line 62, Before sending to queue
producer(), line 80, Received vaconsumer(), line 80, Received value is 0
consumer(), line 77, Before receive value from queue
producer(), line 62, Before sending to queue
produmer(), line 80, Received vaconsumer(), line 80, Received value is 0
consumer(), line 77, Before receive value from queue
This situation it will start whatever tasks in the listlist first and then it will timeslice (CPU share between two tasks)

Additional question:
What is the purpose of the block time during xQueueReceive()?
Answer : To let Task blocks for a period of constant time (know as ticks time ) to wait for any item sent into that
queue 
What if you use ZERO block time during xQueueReceive()? 
Answer : The task will return immediately without waiting
for an item if the queue is empty

*/
static QueueHandle_t switch_queue;

typedef enum { switch__off, switch__on } switch_e;
switch_e get_switch_input_from_switch0();
// TODO: Create this task at PRIORITY_LOW
void producer(void *p) {
  while (1) {
    // This xQueueSend() will internally switch context to "consumer" task because it is higher priority than this
    // "producer" task Then, when the consumer task sleeps, we will resume out of xQueueSend()and go over to the next
    // line

    // TODO: Get some input value from your board
    const switch_e switch_value = get_switch_input_from_switch0();

    // TODO: Print a message before xQueueSend()
    printf("%s(), line %d, Before sending to queue\n", __FUNCTION__, __LINE__);
    // Note: Use printf() and not fprintf(stderr, ...) because stderr is a polling printf
    xQueueSend(switch_queue, &switch_value, 0);
    // TODO: Print a message after xQueueSend()
    printf("%s(), line %d, Queue sent\n", __FUNCTION__, __LINE__);

    vTaskDelay(1000);
  }
}

// TODO: Create this task at PRIORITY_HIGH
void consumer(void *p) {
  switch_e switch_value;
  while (1) {
    // TODO: Print a message before xQueueReceive()
    printf("%s(), line %d, Before receive value from queue\n", __FUNCTION__, __LINE__);
    xQueueReceive(switch_queue, &switch_value, portMAX_DELAY);
    // TODO: Print a message after xQueueReceive()
    printf("%s(), line %d, Received value is %d \n", __FUNCTION__, __LINE__, switch_value);
  }
}
void cli_task() { sj2_cli__init(); }
void main(void) {
  cli_task();
  // Initialize switch
  gpio__construct_as_input(0, 1);
  // TODO: Create your tasks
  xTaskCreate(producer, "producer1", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(consumer, "consumer1", (4096) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // TODO Queue handle is not valid until you create it
  switch_queue =
      xQueueCreate(1, sizeof(switch_e)); // Choose depth of item being our enum (1 should be okay for this example)
  vTaskStartScheduler();
}

switch_e get_switch_input_from_switch0() {
  switch_e value;
  gpio_s signal_switch = {0, 1};
  if (gpio__get(signal_switch)) { // active low switch
    value = switch__off;
  } else
    value = switch__on;
  return value;
}


// Watch-Dog Lab 
#include "FreeRTOS.h"
#include "acceleration.h"
#include "adc.h"
#include "als.h"
#include "board_io.h"
#include "common_macros.h"
#include "event_groups.h"
#include "ff.h"
#include "gpio.h"
#include "gpio_isr.h"
#include "gpio_lab.h"
#include "i2c.h"
#include "i2c_slave_init.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "periodic_scheduler.h"
#include "pwm1.h"
#include "queue.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "task.h"
#include "uart_lab.h"
#include <ssp2.h>
#include <stdio.h>
#include <string.h>
#include <uart.h>
#define CONSUMER_BIT_0 (1 << 0)
#define PRODUCER_BIT_1 (1 << 1)

static QueueHandle_t handle;
EventGroupHandle_t xWatchdog;
EventBits_t xWatchdog_bits;

void watchdog_task();
void consumer_task();
void producer_task();
// Sample code to write a file to the SD Card
void write_file_using_fatfs_pi(uint32_t data_light, int count) {
  const char *filename = "sensor.txt";
  FIL file; // File handle
  UINT bytes_written = 0;
  FRESULT result = f_open(&file, filename, (FA_WRITE | FA_OPEN_APPEND));
  if (FR_OK == result) {
    char string[64];
    // sprintf(string, "Value x : %d, y: %d, z : %d, time: %i\n", axis_data->x, axis_data->y, axis_data->z, &count);
    sprintf(string, "%i, %li\n", count, data_light);
    if (FR_OK == f_write(&file, string, strlen(string), &bytes_written)) {
    } else {
      printf("ERROR: Failed to write data to file\n");
    }
    f_close(&file);
  } else {
    printf("ERROR: Failed to open: %s\n", filename);
  }
}

void cli_task() { sj2_cli__init(); }
void main(void) {
  xWatchdog = xEventGroupCreate();
  cli_task();
  // Initialize switch
  LPC_IOCON->P0_10 |= (0b1 << 1);
  LPC_IOCON->P0_11 |= (0b1 << 1);
  i2c__initialize(I2C__2, 100UL * 1000UL, clock__get_core_clock_hz());
  i2c2__slave_init(4);
  // als__init();
  // TODO: Create your tasks
  // xTaskCreate(producer_task, "producer1", (4096) / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  // xTaskCreate(watchdog_task, "watchdog1", (4096) / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(consumer_task, "consumer1", (4096) / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  // TODO Queue handle is not valid until you create it
  handle =
      xQueueCreate(10, sizeof(uint32_t)); // Choose depth of item being our enum (1 should be okay for this example)
  vTaskStartScheduler();
}

void producer_task(void *params) {
  while (1) {
    // Assume 100ms loop - vTaskDelay(100)
    // Sample code:
    // acceleration__axis_data_s axis_data, avg_axis_data;
    // axis_data = acceleration__get_data();
    uint32_t data_sum;
    uint32_t data_avg;
    uint8_t divider = 100;
    for (int i = 0; i < 100; i++) {
      data_sum = data_sum + als__get_data();
      data_avg = data_sum / divider;
    }
    // printf("data sum %li\n", data_sum);
    // printf("data avg %li\n", data_avg);
    xQueueSend(handle, &data_avg, 0);
    data_sum = 0;
    data_avg = 0;
    xEventGroupSetBits(xWatchdog, PRODUCER_BIT_1);
    vTaskDelay(100);
  }
}

void consumer_task(void *params) {
  while (1) {
    // Assume 100ms loop
    // No need to use vTaskDelay() because the consumer will consume as fast as production rate
    // because we should block on xQueueReceive(&handle, &item, portMAX_DELAY);
    // Sample code:
    uint32_t data_light;
    int count = xTaskGetTickCount();
    if (xQueueReceive(handle, &data_light, 200)) {
      write_file_using_fatfs_pi(data_light, count);
    }
    // Wait forever for an item
    // Do something to write data to the file
    // printf("Value x : %d, y: %d, z : %d, time : %i \n", axis_data.x, axis_data.y, axis_data.z, count);
    // printf("Light sensor value: %li\n", data_light);
    xEventGroupSetBits(xWatchdog, CONSUMER_BIT_0);
  }
}

void watchdog_task(void *params) {
  while (1) {
    vTaskDelay(200);
    // We either should vTaskDelay, but for better robustness, we should
    // block on xEventGroupWaitBits() for slightly more than 100ms because
    // of the expected production rate of the producer() task and its check-in
    xWatchdog_bits = xEventGroupWaitBits(xWatchdog, 0x03, pdTRUE, pdFALSE, 300);
    if ((xWatchdog_bits & (CONSUMER_BIT_0 | PRODUCER_BIT_1)) == (CONSUMER_BIT_0 | PRODUCER_BIT_1)) { // TODO
      printf("BOTH TASKS WERE SET\n");
    } else if ((xWatchdog_bits & CONSUMER_BIT_0) != 0) {
      printf("Consumer set, Producer unset\n");
    } else if ((xWatchdog_bits & PRODUCER_BIT_1) != 0) {
      printf("Producer set, Consumer unset\n");
    } else {
      printf("Consumer, Producer unset\n");
    }
  }
}


// I2C code
#include "FreeRTOS.h"
#include "acceleration.h"
#include "adc.h"
#include "als.h"
#include "board_io.h"
#include "common_macros.h"
#include "event_groups.h"
#include "ff.h"
#include "gpio.h"
#include "gpio_isr.h"
#include "gpio_lab.h"
#include "i2c.h"
#include "i2c_slave_functions.h"
#include "i2c_slave_init.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "periodic_scheduler.h"
#include "pwm1.h"
#include "queue.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "task.h"
#include "uart_lab.h"
#include <ssp2.h>
#include <stdio.h>
#include <string.h>
#include <uart.h>

extern volatile uint8_t slave_memory[256];
void main(void) {
  sj2_cli__init();
  LPC_IOCON->P0_10 |= (0b1 << 1);
  LPC_IOCON->P0_11 |= (0b1 << 1);
  i2c__initialize(I2C__2, 100UL * 1000UL, clock__get_core_clock_hz());
  i2c2__slave_init(4);
  while (1) {
    // if (slave_memory[0] == 0) {
    //   turn_on_an_led(); // TODO
    // } else {
    //   turn_off_an_led(); // TODO
    // }
    // of course, your slave should be more creative than a simple LED on/off
  }

  return -1;
}

void turn_on_an_led() {}

void turn_off_an_led() {}



// I2C code 

// I2C code
#include "FreeRTOS.h"
#include "acceleration.h"
#include "adc.h"
#include "als.h"
#include "board_io.h"
#include "common_macros.h"
#include "event_groups.h"
#include "ff.h"
#include "gpio.h"
#include "gpio_isr.h"
#include "gpio_lab.h"
#include "i2c.h"
#include "i2c_slave_functions.h"
#include "i2c_slave_init.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "periodic_scheduler.h"
#include "pwm1.h"
#include "queue.h"
#include "semphr.h"
#include "sj2_cli.h"
#include "task.h"
#include "uart_lab.h"
#include <ssp2.h>
#include <stdio.h>
#include <string.h>
#include <uart.h>

extern volatile uint8_t slave_memory[256];
void main(void) {
  sj2_cli__init();
  LPC_IOCON->P0_10 |= (0b1 << 1);
  LPC_IOCON->P0_11 |= (0b1 << 1);
  LPC_IOCON->P1_18 |= (0b1 << 0);
  i2c__initialize(I2C__2, 100UL * 1000UL, clock__get_core_clock_hz());
  als__init();
  i2c2__slave_init(0x04);
  gpio0__set_as_output(1, 18);
  while (1) {
    if (slave_memory[0] == 1) {
      turn_on_an_light_sensor(); // TODO
    }
    // of course, your slave should be more creative than a simple LED on/off
    for (int i = 0; i < 256; i++) {
      i2c2__slave_init(0x04);
      if (slave_memory[i] != 0)
        printf("Value at %d : %d \n", i, slave_memory[i]);
    }
    delay__ms(1000);
  }
  return -1;
}

void turn_on_an_light_sensor() {
  uint16_t data;
  uint16_t data_avg;
  for (int i = 0; i < 100; i++) {
    data += als__get_data();
  }
  data_avg = data / 100;
  printf("Sensor value: %d \n", data_avg);
}
